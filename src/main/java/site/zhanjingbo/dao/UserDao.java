package site.zhanjingbo.dao;

import java.util.List;

import site.zhanjingbo.model.User;

public interface UserDao {
	public void reset();
	public List<User> getUserList();
	public void updateMoney(Long userId, double count);
}
