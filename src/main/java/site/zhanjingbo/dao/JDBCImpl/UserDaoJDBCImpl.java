package site.zhanjingbo.dao.JDBCImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import site.zhanjingbo.dao.UserDao;
import site.zhanjingbo.model.User;

@Component("UserJDBCDao")
public class UserDaoJDBCImpl implements UserDao {
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	public void reset() {
		this.jdbcTemplate.execute("update UserBalance set balance=1000");
	}
	public List<User> getUserList() {
		return this.jdbcTemplate.query("select * from UserBalance", new RowMapper<User>() {
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setUserId(rs.getLong("userId"));
				user.setBalance(rs.getDouble("balance"));
				return user;
			}
		});
	}
	public void updateMoney(Long userId, double count) {
		this.jdbcTemplate.update("update UserBalance set balance=balance+? where userId=?", count, userId);
	}
}
