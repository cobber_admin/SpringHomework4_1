package site.zhanjingbo.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import site.zhanjingbo.model.User;
import site.zhanjingbo.service.UserService;

public class Application {
	public static void main(String[] args) {
		//加载Spring
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		//获取UserService实例
		UserService userService = applicationContext.getBean(UserService.class);
		userService.reset();
		try {
			userService.transferMoney(new Long(10000), new Long(10001), 100);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		List<User> userList = userService.getUserList();
		for (User user : userList) {
			System.out.println(user);
		}

		((ConfigurableApplicationContext) applicationContext).close();
	}

}
