package site.zhanjingbo.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import site.zhanjingbo.dao.UserDao;
import site.zhanjingbo.model.User;

@Component
public class UserService {
	// jdbc实现
	@Resource(name = "UserJDBCDao")
	// MyBatis实现
	// @Resource
	private UserDao userDao;

	public void reset() {
		userDao.reset();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void transferMoney(Long srcUserId, Long targetUserId, double count) {
		userDao.updateMoney(srcUserId, 0 - count);
		this.thorwException();
		userDao.updateMoney(targetUserId, count);
	}

	public List<User> getUserList() {
		return userDao.getUserList();
	}

	private void thorwException() {
		throw new RuntimeException("break");
	}
}
